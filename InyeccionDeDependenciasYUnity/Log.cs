﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InyeccionDeDependenciasYUnity
{

    public interface ILog
    {
        void Log(string msg);
    }
    public class LogFile : ILog
    {
        public void Log(string msg)
        {
            Console.WriteLine(msg);
        }
    }

}

    
