﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InyeccionDeDependenciasYUnity
{
    public class HomeController 
    {
        private readonly ILog log;
        public HomeController(ILog log)
        {
            this.log = log;
        }
        public int Index()
        {
            log.Log("Action: Index");

            //return View();
            return 0;
        }
    }
}
