﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InyeccionDeDependenciasYUnity
{
    class Program
    {
        static void Main(string[] args)
        {

            //  Declaramos  un  contenedor  Unity
            var unityContainer = new UnityContainer();
            //  Registramos  IGame  para  que  cuando  se  detecte  la  dependencia
            //  proporcione  una  instancia  de  TrivialPursuit
            unityContainer.RegisterType<IGame, TrivialPursuit>();

            // Hacemos que Unity resuelva la interfaz, proporcionando una instancia
            // de la clase TrivialPursuit
            var game = unityContainer.Resolve<IGame>();

            // Comprobamos que el funcionamiento es correcto
            game.addPlayer();
            game.addPlayer();
            Console.WriteLine(string.Format("{0} personas están jugando a {1}", game.CurrentPlayers, game.Name));
            Console.ReadLine();
            Console.WriteLine("Pausa");

        }
    }
}
